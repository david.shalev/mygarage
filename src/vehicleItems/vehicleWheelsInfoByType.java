package vehicleItems;

public class vehicleWheelsInfoByType {
    private int numOfWheels;
    private int maxAirPressure;
    private String manuFacturer;

    public vehicleWheelsInfoByType(int numOfWheels,int maxAirPressure,String manuFacturer){
        this.numOfWheels = numOfWheels;
        this.maxAirPressure = maxAirPressure;
        this.manuFacturer = manuFacturer;
    }

    public int getNumOfWheels() {
        return numOfWheels;
    }

    public String getManuFacturer() {
        return manuFacturer;
    }

    public int getMaxAirPressure() {
        return maxAirPressure;
    }
}
