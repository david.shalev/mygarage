package vehicleItems;
import enums.*;

public class fuelData {
    private fuelType fuelType;
    private float currentFuelByLitters;
    private float fullTank;

    public fuelData(fuelType fuelType, float currentFuelByLitters, float fullTank) {
        this.fuelType = fuelType;
        this.currentFuelByLitters = currentFuelByLitters;
        this.fullTank = fullTank;
    }

    public fuelType getFuelType() {
        return this.fuelType;
    }

    public float getCurrentFuelByLitters() {
        return this.currentFuelByLitters;
    }

    public float getFullTank() {
        return this.fullTank;
    }

    public void setCurrentFuelByLitters(float amountToRefuel) {
        if (currentFuelByLitters + amountToRefuel > this.fullTank) {
            System.out.println("you ask to refuel more than the maximum: " + fullTank);
        }
        this.currentFuelByLitters = Math.min(currentFuelByLitters + amountToRefuel, fullTank);
    }
}
