package vehicleItems;

public class wheel {
    private String manuFacturer;
    private float currentAirPressure;
    private float maxAirPressure;

    public wheel(String manuFacturer,float currentAirPressure,float maxAirPressure){
        this.manuFacturer =manuFacturer;
        this.currentAirPressure = currentAirPressure;
        this.maxAirPressure= maxAirPressure;
    }

    public String getManuFacturer() {
        return manuFacturer;
    }

    public float getCurrentAirPressure() {
        return currentAirPressure;
    }

    public float getMaxAirPressure() {
        return maxAirPressure;
    }

    public void inflateToMax(){
        currentAirPressure=maxAirPressure;
    }
}
