package enums;

public enum vehicleStatus {
    None,
    InRepair,
    Repaired,
    Paid
}
