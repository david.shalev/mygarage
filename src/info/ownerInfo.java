package info;
import enums.*;

public class ownerInfo {
    private String ownerName;
    private int ownerPhone;
    private vehicleStatus vehicleStatus;

    public ownerInfo(String ownerName,int ownerPhone){
        this.ownerName =ownerName;
        this.ownerPhone=ownerPhone;
        this.vehicleStatus = vehicleStatus.None;
    }

    public vehicleStatus getVehicleStatus() {
        return vehicleStatus;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public int getOwnerPhone() {
        return ownerPhone;
    }

    public void setVehicleStatus(enums.vehicleStatus vehicleStatus) {
        this.vehicleStatus = vehicleStatus;
    }
}
