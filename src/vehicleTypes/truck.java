package vehicleTypes;
import enums.*;
import vehicleItems.wheel;
import java.util.ArrayList;
import info.ownerInfo;
public class truck extends vehicle {
    private boolean isCarryingDangeroesMaterials;
    private float cargoCapacity;

    public truck(boolean isCarringDangeroesMaterials,float cargoCapacity, ownerInfo ownerInfo,
                 String model,int licenseNum,String wheelManufacturer,float currentFuelByLitters)
    {
        super(ownerInfo, model, licenseNum,12,26,wheelManufacturer,fuelType.Soler,
                currentFuelByLitters,110);
        this.isCarryingDangeroesMaterials = isCarringDangeroesMaterials;
        this.cargoCapacity = cargoCapacity;
    }

    public String toString(){
        String vehicleDetailsToShow = super.toString();
        vehicleDetailsToShow += "\n vehicle type:truck is carring dangeroes materials: " +
                this.isCarryingDangeroesMaterials + "\n cargo capacity: " +this.cargoCapacity;
        return vehicleDetailsToShow;
    }
}
