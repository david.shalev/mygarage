package vehicleTypes;
import info.ownerInfo;
import vehicleItems.wheel;

import java.util.ArrayList;
import enums.*;

public class bike extends vehicle {
    private int engineCapacity;
    private bikeLicenseType licenseType;

    public bike(int engineCapacity, bikeLicenseType licenseType,
    ownerInfo ownerInfo, String model, int licenseNum,String wheelManufacturer,
                float currentFuelByLitters )
    {
        super(ownerInfo, model, licenseNum,2,33,wheelManufacturer,fuelType.Ocatn95,
                currentFuelByLitters,8);
        this.engineCapacity = engineCapacity;
        this.licenseType = licenseType;
    }

    public String toString(){
        String vehicleDetailsToShow = super.toString();
        vehicleDetailsToShow += "\n vehicle type:bike engine capacity: " +
                this.engineCapacity + "\n bike license type: " + this.licenseType;
        return vehicleDetailsToShow;
    }
}
