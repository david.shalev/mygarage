package vehicleTypes;
import vehicleItems.*;
import java.util.ArrayList;
import info.ownerInfo;
import enums.*;

public abstract class vehicle {
    private ownerInfo ownerInfo;
    private String model;
    private int licenseNum;
    private fuelData fuelData;
    private ArrayList<wheel> wheels;

    public vehicle(ownerInfo ownerInfo, String model, int licenseNum, int numOfWheels, int wheelMaxAirPressure, String wheelManufacturer, fuelType fuelType, float currentFuelByLitters, float fullTank) {
        this.ownerInfo = ownerInfo;
        this.model = model;
        this.licenseNum = licenseNum;
        this.fuelData = new fuelData(fuelType, currentFuelByLitters, fullTank);
        vehicleWheelsInfoByType wheelsData = new vehicleWheelsInfoByType(numOfWheels, wheelMaxAirPressure, wheelManufacturer);
        this.wheels = setWheels(wheelsData);
    }

    public int getLicenseNum() {
        return this.licenseNum;
    }

    public ownerInfo getOwnerInfo() {
        return this.ownerInfo;
    }

    public ArrayList<wheel> getWheels() {
        return wheels;
    }

    public float getCurrentFuelByLitters() {
        return this.fuelData.getCurrentFuelByLitters();
    }

    public fuelData getFuelData() {
        return this.fuelData;
    }


    public void refuel(fuelType fuelType, float amountToRefuel) {
        if (this.fuelData.getFuelType() == fuelType) {
            this.getFuelData().setCurrentFuelByLitters(amountToRefuel);
        } else {
            System.out.println("the fuelType of this vehicle is " + this.fuelData.getFuelType());
        }
    }

    private static ArrayList<wheel> setWheels(vehicleWheelsInfoByType wheelsData) {
        ArrayList<wheel> Wheels = new ArrayList<wheel>();
        for (int i = 0; i < wheelsData.getNumOfWheels(); i++) {
            Wheels.add(new wheel(wheelsData.getManuFacturer(), wheelsData.getMaxAirPressure(), wheelsData.getMaxAirPressure()));
        }
        return Wheels;
    }

    public String toString() {
        String vehicleDetailsToShow = " license number: " + this.licenseNum + "\n model: " + this.model + "\n owner name: " + this.getOwnerInfo().getOwnerName() + "\n owner phone: " + this.ownerInfo.getOwnerPhone() + "\n vehicle status: " + this.ownerInfo.getVehicleStatus() + "\n fuel type: " + this.fuelData.getFuelType() + "\n current fuel: " + this.fuelData.getCurrentFuelByLitters() + "\n full tank: " + this.fuelData.getFullTank() + "\n number of wheels: " + this.wheels.size() + "\n wheel max air pressure: " + this.wheels.get(0).getMaxAirPressure() + "\n current wheels air pressure: " + this.wheels.get(0).getCurrentAirPressure() + "\n wheel manufacturer: " + this.wheels.get(0).getManuFacturer();
        return vehicleDetailsToShow;
    }
}
