package vehicleTypes;
import info.ownerInfo;
import vehicleItems.wheel;
import enums.*;
import java.util.ArrayList;

public class car extends vehicle {
    private carNumOfDoors numOfDoors;
    private carColor carColor;

    public car(carNumOfDoors numOfDoors,carColor carColor,ownerInfo ownerInfo,
          String model,int licenseNum,String wheelManufacturer,float currentFuelByLitters)
    {
        super(ownerInfo, model, licenseNum,4,31,wheelManufacturer,fuelType.Octan96,
                currentFuelByLitters,55);
        this.numOfDoors = numOfDoors;
        this.carColor = carColor;
    }

    public String toString(){
        String vehicleDetailsToShow = super.toString();
        vehicleDetailsToShow += "\n vehicle type:car" +
                "\n number of doors: " + this.numOfDoors + "\n color: " +this.carColor;
        return vehicleDetailsToShow;
    }
}
