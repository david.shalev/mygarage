package consoleUi;

public class menues {
    public static void showCommandsMenu() {
        System.out.println("Welcome to Garage:");
        System.out.println("Please Select one of the following option:");
        System.out.println("1. Add a new Vehicle");
        System.out.println("2. Get all vehicle license numbers in the garage");
        System.out.println("3. Change Vehicle status");
        System.out.println("4. Inflate wheels of a vehicle");
        System.out.println("5. Refuel vehicle");
        System.out.println("6. Get Overall details of a certain vehicle");
        System.out.println("7. Exit app");
    }

    public static void showVehicleTypesMenu() {
        System.out.println("Please select your vehicle type:");
        System.out.println("1. Car");
        System.out.println("2. bike");
        System.out.println("3. Truck");
    }

    public static void showCarColorsMenu() {
        System.out.println("Please choose your car's color:/n");
        System.out.println("1. Red/n");
        System.out.println("2. Blue/n");
        System.out.println("3. Black/n");
        System.out.println("4. Grey");
    }

    public static void showDoorsNumberMenu() {
        System.out.println("How many doors does your car have?");
        System.out.println("1. 2 Doors");
        System.out.println("2. 3 Doors");
        System.out.println("3. 4 Doors");
        System.out.println("4. 5 Doors");
    }

    public static void showBikeLicenseTypeMenu() {
        System.out.println("Please choose your bike lisence type:");
        System.out.println("1. A");
        System.out.println("2. A1");
        System.out.println("3. A2");
        System.out.println("4. B");
    }

    public static void askIfCarryingToxins() {
        System.out.println("Does the truck carry toxins? (Y/N)");

    }

    public static void showStatusToFilterByMenu() {
        System.out.println("Please choose one the following options:");
        System.out.println("1. Vehicles In Fix");
        System.out.println("2. Vehicles Fixed");
        System.out.println("3. Vehicles Paid");
        System.out.println("4. All Vehicles");
    }

    public static void showVehicleStatusMenu() {
        System.out.println("lease choose the status to undate:");
        System.out.println("1. In Fix");
        System.out.println("2. Fixed");
        System.out.println("3. Paid");
    }

    public static void showFuelTypeMenu() {
        System.out.println("Please select your Vehicle's fuel type:");
        System.out.println("1. Octan 95");
        System.out.println("2. Octan 96");
        System.out.println("3. Octan 98");
        System.out.println("4. Soler");
    }
}
