package consoleUi;
import enums.*;
import info.ownerInfo;
import vehicleTypes.*;
import  java.util.*;

public class GarageManager {
    private ArrayList<vehicle> garageVehicles;

    public GarageManager() {
        this.garageVehicles = new ArrayList<vehicle>();
    }

    public void run() {
        int userChoice = inputValidation.commandsInputValidation();
        switch (userChoice) {
            case 1:
                this.addNewVehicle();
                break;
            case 2:
                this.getAllVehiclesInGarage();
                break;
            case 3:
                this.changeVehicleStatus();
                break;
            case 4:
                this.inflateWheelsOfVehicle();
                break;
            case 5:
                this.refuelVehicle();
                break;
            case 6:
                this.getDetailsOfVehicle();
                break;
            case 7:
                break;
        }
    }


    private void getDetailsOfVehicle() {
        while (true) {
            int licenseNumber = inputValidation.licenseNumberValidation();
            vehicle vehicle = this.findVehicleByLicenseNumber(licenseNumber);
            if (vehicle == null) continue;
            System.out.println(vehicle.toString());
            if (inputValidation.moveHomePage()) {
                run();
                break;
            }
        }
    }

    private void refuelVehicle() {
        vehicle vehicle;
        while (true) {
            int licenseNumber = inputValidation.licenseNumberValidation();
            vehicle = this.findVehicleByLicenseNumber(licenseNumber);
            if (vehicle != null) break;
        }

        fuelType vehicleFuelType = vehicle.getFuelData().getFuelType();
        float currentFuelByLitters = vehicle.getFuelData().getCurrentFuelByLitters();
        System.out.println("the vehicle fuelType is " + vehicleFuelType);
        System.out.println("the amount of fuel at the moment: " + currentFuelByLitters);
        float amountToRefuel = inputValidation.amountToRefuelValidation();
        vehicle.refuel(vehicleFuelType, amountToRefuel);
        System.out.println("successfully refueled to " + vehicle.getFuelData().getCurrentFuelByLitters());
        if (inputValidation.moveHomePage()) {
            run();
        }
    }

    private void inflateWheelsOfVehicle() {
        while (true) {
            int licenseNumber = inputValidation.licenseNumberValidation();
            vehicle vehicle = this.findVehicleByLicenseNumber(licenseNumber);
            if (vehicle == null) continue;
            for (int i = 0; i < vehicle.getWheels().size(); i++) {
                vehicle.getWheels().get(i).inflateToMax();
            }
            System.out.println("you successfully inflated all wheels to maximum");
            if (inputValidation.moveHomePage()) {
                run();
                break;
            }
        }

    }

    private void changeVehicleStatus() {
        vehicle vehicle;
        while (true) {
            int licenseNumber = inputValidation.licenseNumberValidation();
            vehicle = findVehicleByLicenseNumber(licenseNumber);
            if (vehicle != null) break;
        }

        vehicleStatus lastVehicleStatus = vehicle.getOwnerInfo().getVehicleStatus();
        System.out.println("the last vehicle status was " + lastVehicleStatus);
        switch (lastVehicleStatus) {
            case None:
                vehicle.getOwnerInfo().setVehicleStatus(vehicleStatus.InRepair);
            case InRepair:
                vehicle.getOwnerInfo().setVehicleStatus(vehicleStatus.Repaired);
                break;
            case Repaired:
                vehicle.getOwnerInfo().setVehicleStatus(vehicleStatus.Paid);
                break;
            case Paid:
                vehicle.getOwnerInfo().setVehicleStatus(vehicleStatus.None);
                break;
        }
        System.out.println("you successfully change the vehicle status to: " + vehicle.getOwnerInfo().getVehicleStatus());
        if (inputValidation.moveHomePage()) {
            run();
        }
    }

    private void addNewVehicle() {
        vehicleType vehicleType = null;
        ownerInfo ownerInfo;
        String model;
        int licenseNumber = 0;
        String wheelManufacturer;
        float currentFuelByLitters;
        int selectedVehicleType = inputValidation.vehicleTypeValidation();
        switch (selectedVehicleType) {
            case 1:
                vehicleType = vehicleType.Car;
                break;
            case 2:
                vehicleType = vehicleType.Bike;
                break;
            case 3:
                vehicleType = vehicleType.Truck;
                break;
        }
        String ownerName = inputValidation.getStringInput("owner name");
        int phoneNumber = inputValidation.ownerPhoneValidation();
        ownerInfo = new ownerInfo(ownerName, phoneNumber);
        model = inputValidation.getStringInput("model");
        while (true) {
            licenseNumber = inputValidation.licenseNumberValidation();
            if (inputValidation.isLicenseNumberLegal(licenseNumber, garageVehicles)) break;
        }
        wheelManufacturer = inputValidation.getStringInput("wheel manufacturer");
        float fuelPercentage = inputValidation.fuelPercentageValidation();

        switch (vehicleType) {
            case Car:
                carNumOfDoors carNumOfDoors = inputValidation.carNumOfDoorsValidation();
                carColor carColor = inputValidation.carColorValidation();
                currentFuelByLitters = (float) (fuelPercentage * 0.01 * 55);
                car car = new car(carNumOfDoors, carColor, ownerInfo, model, licenseNumber, wheelManufacturer, currentFuelByLitters);
                this.garageVehicles.add(car);
                System.out.println("your car added successfully license number: " + licenseNumber);
                break;
            case Bike:
                int engineCapacity = inputValidation.engineCapacityValidation();
                bikeLicenseType bikeLicenseType = inputValidation.bikeLicenseTypeValidation();
                currentFuelByLitters = (float) (fuelPercentage * 0.01 * 8);
                bike bike = new bike(engineCapacity, bikeLicenseType, ownerInfo, model, licenseNumber, wheelManufacturer, currentFuelByLitters);
                this.garageVehicles.add(bike);
                System.out.println("your bike added successfully license number: " + licenseNumber);
                break;
            case Truck:
                boolean isCarryingToxins = inputValidation.isCarryingToxinsValidation();
                float cargoCapacity = inputValidation.cargoCapacityValidation();
                currentFuelByLitters = (float) (fuelPercentage * 0.01 * 110);
                truck truck = new truck(isCarryingToxins, cargoCapacity, ownerInfo, model, licenseNumber, wheelManufacturer, currentFuelByLitters);
                this.garageVehicles.add(truck);
                System.out.println("your truck added successfully license number: " + licenseNumber);
                break;
        }
        if (inputValidation.moveHomePage()) {
            run();
        }
    }

    private void getAllVehiclesInGarage() {
        System.out.println("all license numbers:");
        for (int i = 0; i < this.garageVehicles.size(); i++) {
            System.out.println(garageVehicles.get(i).getLicenseNum());
        }
        System.out.println();
        if (inputValidation.moveHomePage()) {
            run();
        }
    }

    private vehicle findVehicleByLicenseNumber(int licenseNumber) {
        vehicle vehicle = null;
        for (int i = 0; i < garageVehicles.size(); i++) {
            if (garageVehicles.get(i).getLicenseNum() == licenseNumber) {
                vehicle = garageVehicles.get(i);
                return vehicle;
            }
        }
        if (vehicle == null) {
            System.out.println("the license number you gave not match any car" + " in the garage");
        }
        return vehicle;
    }
}

