package consoleUi;
import enums.*;
import vehicleTypes.vehicle;

import java.util.ArrayList;
import java.util.Scanner;

public class inputValidation {

    public static int licenseNumberValidation() {
        while (true) {
            try {
                System.out.println("please type the license number");
                int licenseNumber = Integer.parseInt(new Scanner(System.in).nextLine());
                return licenseNumber;
            } catch (Exception e) {
                System.out.println("the given input is not a number");
            }
        }
    }

    public static boolean isLicenseNumberLegal(int licenseNumber, ArrayList<vehicle> list) {
        boolean isLicenseNumberLegal = true;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).getLicenseNum() == licenseNumber) {
                isLicenseNumberLegal = false;
                System.out.println("already exists a vehicle with the given license number!");
                break;
            }
        }
        return isLicenseNumberLegal;
    }

    public static int commandsInputValidation() {
        while (true) {
            try {
                menues.showCommandsMenu();
                int userChoice = Integer.parseInt(new Scanner(System.in).nextLine());
                if (userChoice < 1 || userChoice > 7) {
                    System.out.println("the number must be on the range(1-7)");
                    continue;
                }
                return userChoice;
            } catch (Exception e) {
                System.out.println("the given input is not a number");
            }
        }
    }

    public static float amountToRefuelValidation() {
        while (true) {
            try {
                System.out.println("please type the amout to refuel");
                float amountToRefuel = Float.parseFloat(new Scanner(System.in).nextLine());
                return amountToRefuel;
            } catch (Exception e) {
                System.out.println("the given input is not a number");
            }
        }
    }

    public static int vehicleTypeValidation() {
        while (true) {
            try {
                menues.showVehicleTypesMenu();
                int userChoice = Integer.parseInt(new Scanner(System.in).nextLine());
                if (userChoice < 1 || userChoice > 3) {
                    System.out.println("the given number is not in the range(1-3)");
                    continue;
                }
                return userChoice;
            } catch (Exception e) {
                System.out.println("the given input is not a number");
            }

        }
    }

    public static int ownerPhoneValidation() {
        while (true) {
            try {
                System.out.println("please type your phone number");
                int phoneNumber = Integer.parseInt(new Scanner(System.in).nextLine());
                return phoneNumber;
            } catch (Exception e) {
                System.out.println("the given input is not a number");
            }
        }
    }

    public static float fuelPercentageValidation() {
        while (true) {
            try {
                System.out.println("please type the current fuel by percentage");
                float fuelPercentage = Float.parseFloat(new Scanner(System.in).nextLine());
                if (fuelPercentage < 0 || fuelPercentage > 100) {
                    System.out.println(" the range is 0-100");
                    continue;
                }
                return fuelPercentage;
            } catch (Exception e) {
                System.out.println("please type a number");
            }
        }
    }

    public static carNumOfDoors carNumOfDoorsValidation() {
        while (true) {
            try {
                menues.showDoorsNumberMenu();
                int numOfDoors = Integer.parseInt(new Scanner(System.in).nextLine());
                if (numOfDoors < 1 || numOfDoors > 4) {
                    System.out.println("the range is 1 to 4");
                    continue;
                }
                switch (numOfDoors) {
                    case 1:
                        return carNumOfDoors.Two;
                    case 2:
                        return carNumOfDoors.Three;
                    case 3:
                        return carNumOfDoors.Four;
                    case 4:
                        return carNumOfDoors.Five;
                }
            } catch (Exception e) {
                System.out.println("the given input is not a number");
            }
        }
    }

    public static carColor carColorValidation() {
        while (true) {
            try {
                menues.showCarColorsMenu();
                int userChoice = Integer.parseInt(new Scanner(System.in).nextLine());
                if (userChoice < 1 || userChoice > 4) {
                    System.out.println("the range is 1 to 4");
                    continue;
                }
                switch (userChoice) {
                    case 1:
                        return carColor.Red;
                    case 2:
                        return carColor.Blue;
                    case 3:
                        return carColor.Black;
                    case 4:
                        return carColor.Grey;
                }
            } catch (Exception e) {
                System.out.println("the given input is not a number");
            }
        }
    }

    public static int engineCapacityValidation() {
        while (true) {
            try {
                System.out.println("please type vehicle engine capacity");
                int engineCapacity = Integer.parseInt(new Scanner(System.in).nextLine());
                return engineCapacity;
            } catch (Exception e) {
                System.out.println("the given input is not a number");
            }
        }
    }

    public static bikeLicenseType bikeLicenseTypeValidation() {
        while (true) {
            try {
                menues.showBikeLicenseTypeMenu();
                int userChoice = Integer.parseInt(new Scanner(System.in).nextLine());
                if (userChoice < 1 || userChoice > 4) {
                    System.out.println("the range is 1 to 4");
                    continue;
                }
                switch (userChoice) {
                    case 1:
                        return bikeLicenseType.A;
                    case 2:
                        return bikeLicenseType.A1;
                    case 3:
                        return bikeLicenseType.A2;
                    case 4:
                        return bikeLicenseType.B;
                }
            } catch (Exception e) {
                System.out.println("the given input is not a number");
            }
        }
    }

    public static boolean isCarryingToxinsValidation() {
        while (true) {
            menues.askIfCarryingToxins();
            String userChoice = new Scanner(System.in).nextLine();
            if (!userChoice.equals("Y") && !userChoice.equals("N")) {
                System.out.println("the only options to choose are Y/N");
                continue;
                //isCarryingToxinsValidation();
            }
            switch (userChoice) {
                case "Y":
                    return true;
                case "N":
                    return false;
            }
        }
    }

    public static float cargoCapacityValidation() {
        while (true) {
            try {
                System.out.println("please type the cargo capacity");
                float cargoCapacity = Float.parseFloat(new Scanner(System.in).nextLine());
                return cargoCapacity;
            } catch (Exception e) {
                System.out.println("the given input is not a number");
            }
        }
    }

    public static String getStringInput(String key) {
        System.out.println("please type " + key);
        String userValue = new Scanner(System.in).nextLine();
        return userValue;
    }

    public static boolean moveHomePage() {
        while (true) {
            System.out.println("if you want to move to the home page type: HOME");
            String userChoice = new Scanner(System.in).nextLine();
            if (!userChoice.equals("HOME")) {
                System.out.println("the only possible answer is HOME");
                continue;
            }
            return true;
        }
    }
}

